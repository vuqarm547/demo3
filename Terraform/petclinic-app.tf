resource "aws_instance" "public-ec2" {
    ami           = var.ami_id
    instance_type = var.instance_type
    subnet_id     = module.vpc.subnet_public_id
    vpc_security_group_ids = [ aws_security_group.app.id ]
    associate_public_ip_address = true

    tags = {
        Name = "petclinic-app"
    }

    depends_on = [ module.vpc.vpc_id, module.vpc.igw_id, aws_db_instance.default ]

    user_data = <<EOF
#!/bin/sh

aws configure set aws_access_key_id ${var.access_key}
aws configure set aws_secret_access_key ${var.secret_key}
aws configure set default.region us-east-2

aws s3 cp s3://demo3-vugar/petclinic.jar .

amazon-linux-extras install -y java-openjdk11

export MYSQL_PASS=1edc2wsx3qaz
export MYSQL_URL=jdbc:mysql://${aws_db_instance.default.address}:3306/mydb
export MYSQL_USER=admin

java -Dspring.profiles.active=mysql -jar petclinic.jar &
EOF
}

resource "aws_security_group" "app" {
  name        = "SecurityGroup"
  description = "SecurityGroup"
  vpc_id      = module.vpc.vpc_id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    description = "SSH"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    description = "HTTPS"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    description = "HTTP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
}
