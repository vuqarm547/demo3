provider "aws" {
    access_key = var.access_key
    secret_key = var.secret_key
    region = var.aws_region
}

module "vpc" {
    source = "./modules"
    vpc_name = "demo3VPC"
    vpc_cidr = "10.0.0.0/16"
    public_cidr = "10.0.0.0/24"
    private1_cidr = "10.0.1.0/24"
    private2_cidr = "10.0.2.0/24"
    private1_az = data.aws_availability_zones.available.names[0]
    private2_az = data.aws_availability_zones.available.names[1]
    public_az = data.aws_availability_zones.available.names[0]
}

data "aws_availability_zones" "available" {
  state = "available"
}
